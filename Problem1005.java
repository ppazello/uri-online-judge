import java.util.Scanner;
import java.util.Locale;

/**
  * Problem Nº 1005
  * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1005
  *
  */

class Problem1005 {
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);
        Double gradeA = sc.nextDouble();
        Double gradeB = sc.nextDouble();
        Double averageNote = ((gradeA * 3.5) + (gradeB * 7.5)) / 11;
        System.out.printf("MEDIA = %.2f\n", averageNote);
    }
}
