import java.util.Scanner;

/**
 * Problem Nº 1019
 * URL : https://www.urionlinejudge.com.br/judge/pt/problems/view/1019
 *
 */

class Problem1019 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int seconds = sc.nextInt();
        int clockMinutes = seconds / 60;
        int clockSeconds = seconds % 60;
        int clockHours = 0;
        clockHours = clockMinutes / 60;
        clockMinutes = clockMinutes % 60;

        System.out.println(clockHours + ":" + clockMinutes + ":" + clockSeconds);
    }
}
