import java.util.Scanner;

/**
  * Problem Nº 1003
  * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1003
  *
  */

class Problem1003 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int a = sc.nextInt();
    int b = sc.nextInt();
    int soma = a + b;
    System.out.println("SOMA = " + soma);
  }
}
