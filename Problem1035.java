import java.util.Scanner;

/**
 * Problem Nº 1035
 * URL : https://www.urionlinejudge.com.br/judge/pt/problems/view/1035
 *
 */

class Problem1035 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int varA = sc.nextInt();
        int varB = sc.nextInt();
        int varC = sc.nextInt();
        int varD = sc.nextInt();

        if(varB > varC 
                && varD > varA 
                && varC + varD > varA + varD 
                && varC > 0
                && varD > 0
                && varA % 2 == 0)
            System.out.println("Valores aceitos");
        else
            System.out.println("Valores nao aceitos");
    }
}
