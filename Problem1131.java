import java.util.Scanner;

/**
  * Problem Nº 1131
  * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1131
  *
  */

class Problem1131 {
  public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inter = 0, gremio = 0, grenais = 0, empates = 0, continua = 1;

        while(continua == 1) {
          int interGols, gremioGols;
          interGols = sc.nextInt();
          gremioGols = sc.nextInt();

          if(interGols > gremioGols) inter += 1;
          else if(gremioGols > interGols) gremio += 1;
          else empates += 1;

          System.out.println("Novo grenal (1-sim 2-nao)");
          grenais += 1;
          continua = sc.nextInt();
        }

        System.out.println(grenais + " grenais");
        System.out.println("Inter:" + inter);
        System.out.println("Gremio:" + gremio);
        System.out.println("Empates:" + empates);

        if(inter > gremio)
          System.out.println("Inter venceu mais");
        else if(gremio > inter)
          System.out.println("Gremio venceu mais");
        else
          System.out.println("Nao houve vencedor");
    }
}
