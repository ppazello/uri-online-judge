import java.util.Scanner;

/**
  * Problem Nº 1016
  * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1016
  *
  */

class Problem1016 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int carrox = 60;
        int carroy = 90;
        int distancia = sc.nextInt();
        int tempoDistancia = (60 * distancia) / (carroy - carrox);
        System.out.println(tempoDistancia + " minutos");
    }
}
