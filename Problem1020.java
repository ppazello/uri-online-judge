import java.util.Scanner;

/**
 * Problem Nº 1020
 * URL : https://www.urionlinejudge.com.br/judge/pt/problems/view/1020
 *
 */

class Problem1020 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int days = sc.nextInt();
        int showYears = 0;
        int showMonths = 0;
        int showDays = 0;

        showYears = days / 365;
        showMonths = (days % 365) / 30;
        showDays = (days % 365) % 30;
        
        System.out.println(showYears + " ano(s)\n" +
                         showMonths  + " mes(es)\n" + 
                         showDays    + " dia(s)");
    }
}
