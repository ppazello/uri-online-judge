import java.util.Scanner;

/**
 * Problem Nº 1021
 * URL : https://www.urionlinejudge.com.br/judge/pt/problems/view/1021
 *
 */

class Problem1021 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double money = sc.nextDouble();
        int intMoney = (int) money;
        int intCents = (int) ((money - intMoney) * 100);
        int oneHundred = 0;
        int fifty = 0;
        int twenty = 0;
        int ten = 0;
        int five = 0;
        int two = 0;
        // coins
        int one = 0;
        int fiftyCents = 0;
        int twentyFiveCents = 0;
        int tenCents = 0;
        int fiveCents = 0;
        int oneCent = 0;  
        
        oneHundred = intMoney / 100;
        fifty  = (intMoney % 100) / 50;
        twenty = ((intMoney % 100) % 50) / 20;
        ten    = (((intMoney % 100) % 50) % 20) / 10;
        five   = ((((intMoney % 100) % 50) % 20) % 10) / 5;
        two    = (((((intMoney % 100) % 50) % 20) % 10) % 5) / 2;
        one    = (((((intMoney % 100) % 50) % 20) % 10) % 5) % 2;
        fiftyCents = intCents / 50;
        twentyFiveCents = (intCents % 50) / 25;
        tenCents  = ((intCents % 50) % 25) / 10;
        fiveCents = (((intCents % 50) % 25) % 10) / 5;
        oneCent   = ((((intCents % 50) % 25) % 10) % 5);

        System.out.println("NOTAS:\n" +
                            oneHundred + " nota(s) de R$ 100.00\n" +
                            fifty + " nota(s) de R$ 50.00\n" +
                            twenty + " nota(s) de R$ 20.00\n" +
                            ten + " nota(s) de R$ 10.00\n" +
                            five + " nota(s) de R$ 5.00\n" +
                            two + " nota(s) de R$ 2.00\n" +
                            "MOEDAS:\n" +
                            one + " moeda(s) de R$ 1.00\n" +
                            fiftyCents + " moeda(s) de R$ 0.50\n" +
                            twentyFiveCents + " moeda(s) de R$ 0.25\n" +
                            tenCents + " moeda(s) de R$ 0.10\n" +
                            fiveCents + " moeda(s) de R$ 0.05\n" +
                            oneCent + " moeda(s) de R$ 0.01");
    }
}
