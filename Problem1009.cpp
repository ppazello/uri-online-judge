/**
  * Problem Nº 1009
  * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1009
  *
  */

#include <iostream>
#define PORCENTAGEM 15
using namespace std;

int main() {
	char nome[20];
	double salario;
	double total_das_vendas_no_mes;

	cin.getline(nome, 20);
	cin >> salario;
	cin >> total_das_vendas_no_mes;

	cout.precision(2);
	cout << fixed << "TOTAL = R$ " << ((total_das_vendas_no_mes * PORCENTAGEM) / 100) + salario << "\n";

	return 0;
}