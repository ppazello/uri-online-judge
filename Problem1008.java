import java.util.Scanner;
import java.util.Locale;

/**
 * Problem Nº 1008
 * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1008
 *
 */

class Problem1008 {
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		int quantity = sc.nextInt();
		double salary = sc.nextDouble();
		double total = salary * quantity;

		System.out.println("NUMBER = " + number);
		System.out.printf("SALARY = U$ %.2f\n", total);
	}
}