import java.util.Scanner;
import java.util.Locale;

/**
  * Problem Nº 1002
  * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1002
  *
  */

class Problem1002 {
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);
        Double raio = sc.nextDouble();
        Double area = 3.14159 * (Math.pow(raio, 2));
        System.out.printf("A=%.4f\n", area);
    }
}
