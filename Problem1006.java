import java.util.Scanner;
import java.util.Locale;

/**
 * Problem Nº 1006
 * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1006
 *
 */

class Problem1006 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Double gradeA = sc.nextDouble();
        Double gradeB = sc.nextDouble();
        Double gradeC = sc.nextDouble();
        Double averageNote = ((gradeA * 2) + (gradeB * 3) + (gradeC * 5)) / 10;
        System.out.printf("MEDIA = %.1f\n", averageNote);
    }
}
