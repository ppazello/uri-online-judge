import java.util.Scanner;

/**
 * Problem Nº 1007
 * URL : https://www.urionlinejudge.com.br/judge/pt/problems/view/1007
 *
 */

class Problem1007 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numA = sc.nextInt();
        int numB = sc.nextInt();
        int numC = sc.nextInt();
        int numD = sc.nextInt();
        int dif = numA * numB - numC * numD;
        System.out.println("DIFERENCA = " + dif);
    }
}
