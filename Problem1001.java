import java.util.Scanner;

/**
  * Problem Nº 1001
  * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1001
  *
  */

class Problem1001 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int x = a + b;
        System.out.println("X = " + x);
    }
}
