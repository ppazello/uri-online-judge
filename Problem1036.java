import java.util.Scanner;

/**
 * Problem Nº 1036
 * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1036
 *
 */

class Problem1036 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double varA = sc.nextDouble();
        double varB = sc.nextDouble();
        double varC = sc.nextDouble();
        int flag = 0;

        double delta = (varB * varB) - (4 * varA * varC); 

        double r =  ((-varB) + Math.sqrt(delta)) / (2 * varA);
        double r1 = ((-varB) - Math.sqrt(delta)) / (2 * varA);

        if(delta > 0 && varA != 0) {
            System.out.printf("R1 = %.5f\n", r);
            System.out.printf("R2 = %.5f\n", r1);
        } else {
            System.out.println("Impossivel calcular");
        }
    }
}
