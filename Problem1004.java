import java.util.Scanner;

/**
  * Problem Nº 1004
  * URL: https://www.urionlinejudge.com.br/judge/pt/problems/view/1004
  *
  */

class Problem1004 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int a = sc.nextInt();
    int b = sc.nextInt();
    int prod = a * b;
    System.out.println("PROD = " + prod);
  }
}
